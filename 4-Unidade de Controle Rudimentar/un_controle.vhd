library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity un_controle is
    port (
            instr	    :   in unsigned(14 downto 0);
            en_jump     :   out std_logic
    );
end entity;

architecture a_un_controle of un_controle is
    signal opcode   :   unsigned(3 downto 0);
begin
    opcode <= instr(14 downto 11);
	
    en_jump <=  '1' when opcode = "1111" else
                '0';
end architecture;