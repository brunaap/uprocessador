library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity state_machine_tb is
end entity;

architecture a_state_machine_tb of state_machine_tb is
    component state_machine
        port(   clk : in std_logic;
                rst : in std_logic;
                wr_en : in std_logic;
                data_out_sm : out std_logic
        );
    end component;
    signal clk, wr_en, rst, data_out_sm     : std_logic;

begin
    uut:    state_machine port map( clk => clk, rst => rst, wr_en => wr_en,
                                    data_out_sm => data_out_sm
                                );
    process -- sinal clock
    begin
        clk <= '0';
        wait for 50 ns;
        clk <= '1';
        wait for 50 ns;
    end process;
    
    process -- sinal reset
    begin
        rst <= '1';
        wait for 100 ns;
        rst <= '0';
        wait for 1000 ns;
        rst <= '1';
        wait;
    end process;
    
    process -- sinais dos casos de teste
	begin
        wr_en <= '0';
		wait for 100 ns;
		wr_en <= '1';
        wait;
    end process;
end architecture;
    
    