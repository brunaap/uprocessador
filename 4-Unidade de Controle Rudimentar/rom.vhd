library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rom is
    port( clk           :  in std_logic;
          endereco      :  in unsigned(7 downto 0); -- define a posicao da ROM que vamos acessar, como vaide 0 a 256 = 8 bits
          dados_rom     :  out unsigned(14 downto 0) -- tamanho de cada dado da ROM
    );
end entity;

architecture a_rom of rom is
    type mem is array (0 to 256) of unsigned(14 downto 0);
    constant conteudo_rom : mem := (
       -- caso endereco => conteudo
       0  => "000000000000010",
       1  => "000100000000000",
       2  => "000000000000000",
       3  => "111100000001001",
       4  => "000100000000000",
       5  => "000000000000010",
       6  => "000111100000011",
       7  => "000000000000010",
       8  => "000000000000010",
       9  => "000000000000000",
       10 => "111100000000000",       
       -- abaixo: casos omissos => (zero em todos os bits)
       others => (others=>'0')
    );
begin
    process(clk)
    begin
        if(rising_edge(clk)) then
            dados_rom <= conteudo_rom(to_integer(endereco));
        end if;
    end process;
end architecture;