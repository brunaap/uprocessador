library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PC_rom_jump is
    port(   wr_en			 		: in std_logic;
			clk				 		: in std_logic;
            rst                     : in std_logic;
            saida_rom				: out unsigned(14 downto 0)
    );      
end entity;

architecture a_PC_rom_jump of PC_rom_jump is
    component PC is
        port(   clk 			: in std_logic;
                wr_en 			: in std_logic;
                rst    			: in std_logic;
                data_in_pc 		: in unsigned(7 downto 0);
                data_out_pc 	: out unsigned(7 downto 0)
        );
    end component;
    component rom is
        port(   clk           	: in std_logic;
                endereco      	: in unsigned(7 downto 0);
                dados_rom		: out unsigned(14 downto 0)
        );
    end component;
	component state_machine is
        port(   clk 			: in std_logic;
                rst 			: in std_logic;
                wr_en 			: in std_logic;
                data_out_sm 	: out std_logic
        );
	end component;
	component un_controle is
		port (
				instr	    :   in unsigned(14 downto 0);
				en_jump     :   out std_logic
		);
	end component;
    
    signal data_in_pc, data_out_pc		: unsigned(7 downto 0);
	signal instr				: unsigned(14 downto 0);
	signal data_out_sm, en_jump			: std_logic;

begin
    PC0: PC port map(   clk=>clk,
                        wr_en=>wr_en,
                        rst=>rst,
                        data_in_pc=>data_in_pc,
                        data_out_pc=>data_out_pc);
                                    
    rom0: rom port map( clk=>clk,
                        endereco=>data_out_pc,
                        dados_rom=>instr);
						
	sm0: state_machine port map( clk=>clk,
                                 wr_en=>wr_en,
                                 rst=>rst,
                                 data_out_sm=>data_out_sm);
								 
	uc0: un_controle port map(	instr=>instr,
								en_jump=>en_jump);
											
	saida_rom	<=	instr when data_out_sm = '0' else "000000000000000";
	
	data_in_pc	<=	instr(7 downto 0) 			when en_jump = '1' else
					data_out_pc + "00000001" 	when data_out_sm = '1' else
					data_out_pc;
end architecture;




