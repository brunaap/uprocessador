library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity state_machine is
	port(   clk : in std_logic;
            rst : in std_logic;
            wr_en : in std_logic;
            data_out_sm : out std_logic
 );
end entity;

architecture a_state_machine of state_machine is
	signal registro: std_logic;
begin
	process(clk,rst,wr_en) -- acionado se houver mudança em clk, rst ou wr_en
	begin
		if rst='1' then
			registro <= '0';
		elsif wr_en='1' then
			if rising_edge(clk) then
				registro<= not registro;
			end if;
		end if;
	end process;

	data_out_sm <= registro; -- conexao direta, fora do processo
end architecture;
