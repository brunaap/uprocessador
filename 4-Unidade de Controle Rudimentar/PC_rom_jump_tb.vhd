library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PC_rom_jump_tb is
end entity;

architecture a_PC_rom_jump_tb of PC_rom_jump_tb is
    component PC_rom_jump is
        port(   wr_en			 		: in std_logic;
                clk				 		: in std_logic;
                rst                     : in std_logic;
                saida_rom				: out unsigned(14 downto 0)
        );
    end component;
    signal clk, wr_en, rst         	:	std_logic;
	signal saida_rom				:	unsigned(14 downto 0);
begin
    uut:    PC_rom_jump port map( clk => clk, wr_en => wr_en,rst => rst,
                                     saida_rom => saida_rom
                                );
    process -- sinal clock
    begin
        clk <= '0';
        wait for 50 ns;
        clk <= '1';
        wait for 50 ns;
    end process;
    
    process -- sinais dos casos de teste
	begin
		wr_en <= '1';
        wait;
    end process;
    
    process -- sinais dos casos de teste
	begin
		rst <= '1';
        wait for 50 ns;
        rst <= '0';
        wait;
    end process;
    
end architecture;
    
    