library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bancoderegistradores is
	port(	read_reg1 : in unsigned(2 downto 0);-- escolhe um registrador dentre os 8 a serem lidos
			read_reg2 : in unsigned(2 downto 0);-- escolhe outro registrador dentre os 8 a serem lidos
			write_data : in unsigned(15 downto 0);-- dado que quero escrever em um registrador
			write_register : in unsigned(2 downto 0);-- escolhe em qual registrador vou escrever write data
			wr_en : in std_logic;
			clk : in std_logic;
			rst : in std_logic;
			read_data1 : out unsigned(15 downto 0);-- dado do registrador escolhido
			read_data2 : out unsigned(15 downto 0)-- dado do outro registrador escolhido
	);
end entity;

architecture a_bancoderegistradores of bancoderegistradores is
	component reg16bits
		port(	clk : in std_logic;
				rst : in std_logic;
				wr_en : in std_logic;
				data_in : in unsigned(15 downto 0);
				data_out : out unsigned(15 downto 0)			
		);
	end component;
	signal out_reg0, out_reg1, out_reg2, out_reg3, out_reg4, out_reg5, out_reg6, out_reg7	:	unsigned(15 downto 0);
	signal in_reg0, in_reg1, in_reg2, in_reg3, in_reg4, in_reg5, in_reg6, in_reg7			:	unsigned(15 downto 0);
begin
	reg0: reg16bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg0, data_out => out_reg0);
	reg1: reg16bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg1, data_out => out_reg1);
	reg2: reg16bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg2, data_out => out_reg2);
	reg3: reg16bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg3, data_out => out_reg3);
	reg4: reg16bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg4, data_out => out_reg4);
	reg5: reg16bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg5, data_out => out_reg5);
	reg6: reg16bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg6, data_out => out_reg6);
	reg7: reg16bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg7, data_out => out_reg7);
	
	read_data1 <= 	out_reg0 when read_reg1 = "000" else
					out_reg1 when read_reg1 = "001" else
					out_reg2 when read_reg1 = "010" else
					out_reg3 when read_reg1 = "011" else
					out_reg4 when read_reg1 = "100" else
					out_reg5 when read_reg1 = "101" else
					out_reg6 when read_reg1 = "110" else
					out_reg7 when read_reg1 = "111" else
					"0000000000000000";
					
	read_data2 <= 	out_reg0 when read_reg2 = "000" else
					out_reg1 when read_reg2 = "001" else
					out_reg2 when read_reg2 = "010" else
					out_reg3 when read_reg2 = "011" else
					out_reg4 when read_reg2 = "100" else
					out_reg5 when read_reg2 = "101" else
					out_reg6 when read_reg2 = "110" else
					out_reg7 when read_reg2 = "111" else
					"0000000000000000";
	
	in_reg0 <= 	write_data when write_register = "000" else
				out_reg0;
	
	in_reg1 <= 	write_data when write_register = "001" else
				out_reg1;
				
	in_reg2 <= 	write_data when write_register = "010" else
				out_reg2;

	in_reg3 <= 	write_data when write_register = "011" else
				out_reg3;
	
	in_reg4 <= 	write_data when write_register = "100" else
				out_reg4;
	
	in_reg5 <= 	write_data when write_register = "101" else
				out_reg5;
	
	in_reg6 <= 	write_data when write_register = "110" else
				out_reg6;
				
	in_reg7 <= 	write_data when write_register = "111" else
				out_reg7;
				
end architecture;