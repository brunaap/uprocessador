library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bancoderegistradores_tb is
end entity;

architecture a_bancoderegistradores_tb of bancoderegistradores_tb is
	component bancoderegistradores 
		port(	read_reg1 : in unsigned(3 downto 0);-- escolhe um registrador dentre os 8 a serem lidos
				read_reg2 : in unsigned(3 downto 0);-- escolhe outro registrador dentre os 8 a serem lidos
				write_data : in unsigned(7 downto 0);-- dado que quero escrever em um registrador
				write_register : in unsigned(3 downto 0);-- escolhe em qual registrador vou escrever write data
				wr_en : in std_logic;
				clk : in std_logic;
				rst : in std_logic;
				read_data1 : out unsigned(7 downto 0);-- dado do registrador escolhido
				read_data2 : out unsigned(7 downto 0)-- dado do outro registrador escolhido
		);
	end component;
	signal clk, rst, wr_en 							: std_logic;
	signal write_data, read_data1, read_data2 		: unsigned(7 downto 0);
	signal write_register, read_reg1, read_reg2 	: unsigned(3 downto 0);
		
begin
	uut:	bancoderegistradores port map(	clk => clk,
								rst => rst,
								wr_en => wr_en,
								write_data => write_data,
								write_register => write_register,
								read_data1 => read_data1,
								read_data2 => read_data2,
								read_reg1 => read_reg1,
								read_reg2 => read_reg2
						);
	
	process -- sinal clock
	begin
		clk <= '0';
		wait for 50 ns;
		clk <= '1';
		wait for 50 ns;
	end process;
	
	process -- sinal reset
	begin
		rst <= '1';
		wait for 100 ns;
		rst <= '0';
		wait for 1100 ns;
		rst <= '1';
		wait for 100 ns;
		rst <= '0';
		wait;
	end process;
	
	process
	begin
		wr_en <= '1';
		write_data <= "00001001";
		write_register <= "0000";
		wait for 100 ns;
		write_data <= "00000001";
		write_register <= "0001";
		wait for 100 ns;
		read_reg1 <= "0000";
		read_reg2 <= "0001";
		wait for 100 ns;
		
		write_data <= "00000010";
		write_register <= "0010";
		read_reg1 <= "0010";
		read_reg2 <= "0111";
		wait for 100 ns;
		
		write_data <= "00000011";
		write_register <= "0011";
		read_reg1 <= "0101";
		read_reg2 <= "0100";
		wait for 100 ns;
		
		write_data <= "00000100";
		write_register <= "0100";
		read_reg1 <= "0100";
		read_reg2 <= "0011";
		wait for 100 ns;

		write_data <= "00000101";
		write_register <= "0101";
		read_reg1 <= "0011";
		read_reg2 <= "0010";
		wait for 100 ns;

		write_data <= "00000110";
		write_register <= "0110";
		read_reg1 <= "0010";
		read_reg2 <= "0001";
		wait for 100 ns;

		write_data <= "00000111";
		write_register <= "0111";
		read_reg1 <= "0001";
		read_reg2 <= "0000";
		wait for 100 ns;

		wr_en <= '0';
		write_data <= "00001000";
		write_register <= "0000";
		read_reg1 <= "0010";
		read_reg2 <= "0001";
		wait for 100 ns;
		write_data <= "11111000";
		write_register <= "0101";
		read_reg1 <= "0101";
		wait for 100 ns;
		wait;
	end process;
end architecture;