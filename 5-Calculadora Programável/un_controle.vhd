library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity un_controle is
    port (
            instr	    :   in unsigned(14 downto 0);
            en_jump     :   out std_logic;
            en_ldi      :   out std_logic;
            en_add      :   out std_logic;
            en_sub      :   out std_logic;
            en_mov      :   out std_logic
    );
end entity;

architecture a_un_controle of un_controle is
    signal opcode   :   unsigned(6 downto 0);
begin
    opcode <= instr(14 downto 8);
	
    en_jump <=  '1' when opcode = "1001010" else
                '0';
    en_ldi  <=  '1' when opcode = "0001110" else
                '0';
    en_add  <=  '1' when opcode = "0000000" else
                '0';
    en_sub  <=  '1' when opcode = "0000100" else
                '0';
    en_mov  <=  '1' when opcode = "0001011" else
                '0';
end architecture;