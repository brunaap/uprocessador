library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rom is
    port( clk           :  in std_logic;
          endereco      :  in unsigned(7 downto 0); -- define a posicao da ROM que vamos acessar, como vaide 0 a 256 = 8 bits
          dados_rom     :  out unsigned(14 downto 0) -- tamanho de cada dado da ROM
    );
end entity;

architecture a_rom of rom is
    type mem is array (0 to 255) of unsigned(14 downto 0);
    constant conteudo_rom : mem := (
       -- caso endereco => conteudo
       0  => "000111000110101",
       1  => "000111001001000",
       2  => "000111001010000",
       3  => "000000001010011",
       4  => "000000001010100",
       5  => "000010001010001",
       6  => "100101000010100",
       7  => "000000000000010",
       8  => "000000000000010",
       9  => "000000000000000",
       10 => "000000000000000",
       20 => "000101100110101",
       21 => "100101000000010",
       -- abaixo: casos omissos => (zero em todos os bits)
       others => (others=>'0')
    );
begin
    process(clk)
    begin
        if(rising_edge(clk)) then
            dados_rom <= conteudo_rom(to_integer(endereco));
        end if;
    end process;
end architecture;