library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity processador is
    port(   clk				 		: in std_logic;
            rst                     : in std_logic
    );      
end entity;

architecture a_processador of processador is
    component PC is
        port(   clk 			: in std_logic;
                wr_en 			: in std_logic;
                rst    			: in std_logic;
                data_in_pc 		: in unsigned(7 downto 0);
                data_out_pc 	: out unsigned(7 downto 0)
        );
    end component;
    component rom is
        port(   clk           	: in std_logic;
                endereco      	: in unsigned(7 downto 0);
                dados_rom		: out unsigned(14 downto 0)
        );
    end component;
	component state_machine is
        port(   clk 			: in std_logic;
                rst 			: in std_logic;
                estado       	: out unsigned(1 downto 0)
        );
	end component;
	component un_controle is
		port (
				instr	    :   in unsigned(14 downto 0);
				en_jump     :   out std_logic;
                en_ldi      :   out std_logic;
                en_add      :   out std_logic;
                en_sub      :   out std_logic;
                en_mov      :   out std_logic
		);
	end component;
    component bancoderegistradores is
        port(	read_reg1 : in unsigned(3 downto 0);-- escolhe um registrador dentre os 8 a serem lidos
                read_reg2 : in unsigned(3 downto 0);-- escolhe outro registrador dentre os 8 a serem lidos
                write_data : in unsigned(7 downto 0);-- dado que quero escrever em um registrador
                write_register : in unsigned(3 downto 0);-- escolhe em qual registrador vou escrever write data
                wr_en : in std_logic;
                clk : in std_logic;
                rst : in std_logic;
                read_data1 : out unsigned(7 downto 0);-- dado do registrador escolhido
                read_data2 : out unsigned(7 downto 0)-- dado do outro registrador escolhido
        );
    end component;
    component ULA is
        port(   entrada_dados0,entrada_dados1    : in unsigned(7 downto 0);
                saida                : out unsigned(7 downto 0);
                seleciona_operacao : in unsigned (1 downto 0)-- soma = 00, sub = 01, div = 10, maiorq = 11
          );
     end component;
    
    signal data_in_pc, data_out_pc		: unsigned(7 downto 0);
	signal instr, saida_rom				        : unsigned(14 downto 0);
	signal en_jump,wr_en_PC,wr_en_br, en_ldi, en_add, en_sub, en_mov			: std_logic;
    signal estado         : unsigned(1 downto 0);
    signal write_data, read_data1, read_data2 		: unsigned(7 downto 0);
	signal write_register, read_reg1, read_reg2 	: unsigned(3 downto 0);
    signal entrada_ULA0,entrada_ULA1,saida_ULA      : unsigned(7 downto 0);
    signal seleciona        :unsigned(1 downto 0);
    signal instr_data1, instr_data2     : unsigned(3 downto 0); -- usados no decode
    signal instr_jmp    : unsigned(7 downto 0); -- usado no decode

begin
    PC0: PC port map(   clk=>clk,
                        wr_en=>wr_en_PC,
                        rst=>rst,
                        data_in_pc=>data_in_pc,
                        data_out_pc=>data_out_pc);
                                    
    rom0: rom port map( clk=>clk,
                        endereco=>data_out_pc,
                        dados_rom=>instr);
						
	sm0: state_machine port map( clk=>clk,
                                 rst=>rst,
                                 estado => estado);
								 
	uc0: un_controle port map(	instr=>instr,
								en_jump=>en_jump,
                                en_ldi=>en_ldi,
                                en_add=>en_add,
                                en_sub=>en_sub,
                                en_mov=>en_mov);
                                
    br0: bancoderegistradores port map ( clk => clk,
                                         rst => rst,
                                         wr_en => wr_en_br,
                                         write_data => write_data,
                                         write_register => write_register,
                                         read_data1 => read_data1,
                                         read_data2 => read_data2,
                                         read_reg1 => read_reg1,
                                         read_reg2 => read_reg2 );
    
    ula0: ULA port map (entrada_dados0=>entrada_ULA0,
                        entrada_dados1=>entrada_ULA1,
                        saida=>saida_ULA,
                        seleciona_operacao=>seleciona);
                        
    -- Fetch
    saida_rom	<=	instr when estado = "00" else "100000000000000";
    
    -- Decode
    instr_data1 <= instr(7 downto 4) when estado = "01" else instr_data1;
    instr_data2 <= instr(3 downto 0) when estado = "01" else instr_data2;
    instr_jmp <= instr(7 downto 0) when estado = "01" else instr_jmp;
    -- decode do opcode foi feito direto na un_controle
    
    --Execute
    -- lDI, MOV e escrever nos registradores
    wr_en_br <= '1' when estado = "10" else '0';
	write_data <= "0000" & instr_data2 when en_ldi = '1' else
                  saida_ULA when en_add = '1' else
                  saida_ULA when en_sub = '1' else
                  read_data2 when en_mov = '1' else
                  "00000000";	
    write_register <= instr_data1 when en_ldi = '1' else
                      instr_data1 when en_add = '1' else
                      instr_data1 when en_sub = '1' else
                      instr_data1 when en_mov = '1' else
                      "0000";
    
    -- ADD, SUB ler dos registradores e ULA
    read_reg1 <= instr_data1 when en_add ='1' else
                 instr_data1 when en_sub ='1' else
                 "0000";
    read_reg2 <= instr_data2 when en_add ='1' else 
                 instr_data2 when en_mov ='1' else
                 "0000";
    seleciona <= "00" when en_add = '1' else
                 "01" when en_sub = '1' else
                 seleciona;
    entrada_ULA0 <= read_data1 when en_add = '1' else
                    read_data1 when en_sub = '1' else
                    "00000000";
    entrada_ULA1 <= read_data2 when en_add = '1' else
                    "0000" & instr_data2 when en_sub = '1' else
                    "00000000";

	
	-- PC
    wr_en_PC <= '1' when estado = "10" else '0';
	data_in_pc	<=	instr_jmp 			when en_jump = '1' else
					data_out_pc + "00000001" 	when estado = "10" else
					data_out_pc;
end architecture;




