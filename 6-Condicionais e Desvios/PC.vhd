library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PC is
	port(   clk : in std_logic;
            wr_en : in std_logic;
            rst : in std_logic;
            data_in_pc : in unsigned(7 downto 0);
            data_out_pc : out unsigned(7 downto 0)
 );
end entity;

architecture a_PC of PC is
    signal registro :   unsigned(7 downto 0);
begin
	process(clk,wr_en,rst) -- acionado se houver mudança em clk, rst ou wr_en
	begin
		if rst='1' then
			registro <= "00000000";
		elsif wr_en='1' then
			if rising_edge(clk) then
				registro <= data_in_pc;
			end if;
		end if;
	end process;

	data_out_pc <= registro; -- conexao direta, fora do processo
end architecture;