library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ULA_tb is
end entity;

architecture a_ULA_tb of ULA_tb is
    component ULA is
        port( entrada_dados0,entrada_dados1    : in unsigned(7 downto 0);
              saida                : out unsigned(7 downto 0);
              seleciona_operacao : in unsigned (1 downto 0)-- soma = 00, sub = 01, div = 10, maiorq = 11
          );
    end component;
    signal entrada0,entrada1,saida      :unsigned(7 downto 0);
    signal seleciona        :unsigned(1 downto 0);
begin
    uut: ULA port map(entrada_dados0=>entrada0,entrada_dados1=>entrada1,saida=>saida,seleciona_operacao=>seleciona);
    process
    begin
		entrada0 <= "00000000";
        entrada1 <= "00000000";
        seleciona <= "00";
        wait for 50 ns;
		seleciona <= "01";
		wait for 50 ns;
		seleciona <= "11";
		wait for 50 ns;
        entrada0 <= "00000001";
        entrada1 <= "00000001";
        seleciona <= "00";
        wait for 50 ns;
		seleciona <= "01";
		wait for 50 ns;
		seleciona <= "10";
		wait for 50 ns;
		seleciona <= "11";
		wait for 50 ns;
	    entrada0 <= "00000011";
        entrada1 <= "00000111";
        seleciona <= "00";
        wait for 50 ns;
		seleciona <= "01";
		wait for 50 ns;
		seleciona <= "10";
		wait for 50 ns;
		seleciona <= "11";
		wait for 50 ns;
		entrada0 <= "00000111";
        entrada1 <= "00000011";
        seleciona <= "00";
        wait for 50 ns;
		seleciona <= "01";
		wait for 50 ns;
		seleciona <= "10";
		wait for 50 ns;
		seleciona <= "11";
		wait for 50 ns;
        wait;
        end process;
end architecture;