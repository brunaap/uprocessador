library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bancoderegistradores is
	port(	read_reg1 : in unsigned(3 downto 0);-- escolhe um registrador dentre os 16 a serem lidos
			read_reg2 : in unsigned(3 downto 0);-- escolhe outro registrador dentre os 16 a serem lidos
			write_data : in unsigned(7 downto 0);-- dado que quero escrever em um registrador
			write_register : in unsigned(3 downto 0);-- escolhe em qual registrador vou escrever write data
			wr_en : in std_logic;
			clk : in std_logic;
			rst : in std_logic;
			read_data1 : out unsigned(7 downto 0);-- dado do registrador escolhido
			read_data2 : out unsigned(7 downto 0)-- dado do outro registrador escolhido
	);
end entity;

architecture a_bancoderegistradores of bancoderegistradores is
	component reg8bits
		port(	clk : in std_logic;
				rst : in std_logic;
				wr_en : in std_logic;
				data_in : in unsigned(7 downto 0);
				data_out : out unsigned(7 downto 0)			
		);
	end component;
	signal out_reg0, out_reg1, out_reg2, out_reg3, out_reg4, out_reg5, out_reg6, out_reg7, out_reg8, out_reg9,
           out_reg10, out_reg11, out_reg12, out_reg13, out_reg14, out_reg15	                :	unsigned(7 downto 0);
	signal in_reg0, in_reg1, in_reg2, in_reg3, in_reg4, in_reg5, in_reg6, in_reg7, in_reg8, in_reg9,
           in_reg10, in_reg11, in_reg12, in_reg13, in_reg14, in_reg15			            :	unsigned(7 downto 0);
begin
	reg0: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg0, data_out => out_reg0);
	reg1: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg1, data_out => out_reg1);
	reg2: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg2, data_out => out_reg2);
	reg3: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg3, data_out => out_reg3);
	reg4: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg4, data_out => out_reg4);
	reg5: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg5, data_out => out_reg5);
	reg6: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg6, data_out => out_reg6);
	reg7: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg7, data_out => out_reg7);
    reg8: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg8, data_out => out_reg8);
    reg9: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg9, data_out => out_reg9);
    reg10: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg10, data_out => out_reg10);
    reg11: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg11, data_out => out_reg11);
    reg12: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg12, data_out => out_reg12);
    reg13: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg13, data_out => out_reg13);
    reg14: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg14, data_out => out_reg14);
    reg15: reg8bits port map(clk => clk, rst => rst, wr_en => wr_en, data_in => in_reg15, data_out => out_reg15);
	
	read_data1 <= 	out_reg0 when read_reg1 = "0000" else
					out_reg1 when read_reg1 = "0001" else
					out_reg2 when read_reg1 = "0010" else
					out_reg3 when read_reg1 = "0011" else
					out_reg4 when read_reg1 = "0100" else
					out_reg5 when read_reg1 = "0101" else
					out_reg6 when read_reg1 = "0110" else
					out_reg7 when read_reg1 = "0111" else
                    out_reg8 when read_reg1 = "1000" else
                    out_reg9 when read_reg1 = "1001" else
                    out_reg10 when read_reg1 = "1010" else
                    out_reg11 when read_reg1 = "1011" else
                    out_reg12 when read_reg1 = "1100" else
                    out_reg13 when read_reg1 = "1101" else
                    out_reg14 when read_reg1 = "1110" else
                    out_reg15 when read_reg1 = "1111" else
					"00000000";
					
	read_data2 <= 	out_reg0 when read_reg2 = "0000" else
					out_reg1 when read_reg2 = "0001" else
					out_reg2 when read_reg2 = "0010" else
					out_reg3 when read_reg2 = "0011" else
					out_reg4 when read_reg2 = "0100" else
					out_reg5 when read_reg2 = "0101" else
					out_reg6 when read_reg2 = "0110" else
					out_reg7 when read_reg2 = "0111" else
                    out_reg8 when read_reg2 = "1000" else
                    out_reg9 when read_reg2 = "1001" else
                    out_reg10 when read_reg2 = "1010" else
                    out_reg11 when read_reg2 = "1011" else
                    out_reg12 when read_reg2 = "1100" else
                    out_reg13 when read_reg2 = "1101" else
                    out_reg14 when read_reg2 = "1110" else
                    out_reg15 when read_reg2 = "1111" else
					"00000000";
	
	in_reg0 <= 	write_data when write_register = "0000" else
				out_reg0;
	
	in_reg1 <= 	write_data when write_register = "0001" else
				out_reg1;
				
	in_reg2 <= 	write_data when write_register = "0010" else
				out_reg2;

	in_reg3 <= 	write_data when write_register = "0011" else
				out_reg3;
	
	in_reg4 <= 	write_data when write_register = "0100" else
				out_reg4;
	
	in_reg5 <= 	write_data when write_register = "0101" else
				out_reg5;
	
	in_reg6 <= 	write_data when write_register = "0110" else
				out_reg6;
				
	in_reg7 <= 	write_data when write_register = "0111" else
				out_reg7;
                
    in_reg8 <= 	write_data when write_register = "1000" else
				out_reg8;
                
    in_reg9 <= 	write_data when write_register = "1001" else
				out_reg9;
                
    in_reg10 <= write_data when write_register = "1010" else
				out_reg10;
                
    in_reg11 <= write_data when write_register = "1011" else
				out_reg11;
                
    in_reg12 <= write_data when write_register = "1100" else
				out_reg12;
                
    in_reg13 <= write_data when write_register = "1101" else
				out_reg13;
                
    in_reg14 <= write_data when write_register = "1110" else
				out_reg14;
                
    in_reg15 <= write_data when write_register = "1111" else
				out_reg15;
                
end architecture;