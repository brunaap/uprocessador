library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity state_machine_tb is
end entity;

architecture a_state_machine_tb of state_machine_tb is
    component state_machine
        port(   clk : in std_logic;
                rst : in std_logic;
                estado  : out unsigned(1 downto 0)
        );
    end component;
    signal clk, rst     : std_logic;
    signal estado_s     : unsigned(1 downto 0);

begin
    sm0:    state_machine port map( clk => clk, rst => rst,
                                estado=>estado_s
                                );
    process -- sinal clock
    begin
        clk <= '0';
        wait for 50 ns;
        clk <= '1';
        wait for 50 ns;
    end process;
    
    process -- sinal reset
    begin
        rst <= '1';
        wait for 100 ns;
        rst <= '0';
        wait;
    end process;
    
end architecture;
    
    