library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ULA is
    port( entrada_dados0,entrada_dados1    	: in unsigned(7 downto 0);
          saida                				: out unsigned(7 downto 0);
		  saida_flags						: out unsigned(7 downto 0);
          seleciona_operacao 				: in unsigned (1 downto 0) -- soma = 00, sub = 01, div = 10, maiorq = 
          );
end entity;

architecture a_ULA of ULA is
begin
    saida <=    entrada_dados0+entrada_dados1 when seleciona_operacao ="00" else
                entrada_dados0-entrada_dados1 when seleciona_operacao ="01" else
                entrada_dados0                when seleciona_operacao ="11" and entrada_dados0>entrada_dados1 else
                entrada_dados1                when seleciona_operacao ="11" and entrada_dados1>entrada_dados0 else
				entrada_dados1                when seleciona_operacao ="11" and entrada_dados1=entrada_dados0 else
                entrada_dados0/entrada_dados1 when seleciona_operacao ="10" else
                "00000000";
			
	
	saida_flags <=	"00000010" when entrada_dados0=entrada_dados1 and seleciona_operacao = "01" else
					"00000100" when entrada_dados0<entrada_dados1 and seleciona_operacao = "01" else
					"00000000";
				


	
end architecture;