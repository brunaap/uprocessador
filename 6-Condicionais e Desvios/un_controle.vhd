library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity un_controle is
    port (
            instr	    :   in unsigned(14 downto 0);
			flags		:	in unsigned(7 downto 0);
            en_jump     :   out std_logic;
            en_ldi      :   out std_logic;
            en_add      :   out std_logic;
            en_sub      :   out std_logic;
            en_mov      :   out std_logic;
            en_addi     :   out std_logic;
			en_cp	    :   out std_logic;
			en_brlt		:	out std_logic 
    );
end entity;

architecture a_un_controle of un_controle is
    signal opcode   :   unsigned(6 downto 0);
    signal opcode_ldi : unsigned(2 downto 0);
begin
    opcode <= instr(14 downto 8);
    opcode_ldi <= instr(14 downto 12);
	
    en_ldi  <=  '1' when opcode_ldi = "111" else
                '0';
    en_jump <=  '1' when opcode = "1001010" else
                '0';
    en_add  <=  '1' when opcode = "0000000" else
                '0';
    en_sub  <=  '1' when opcode = "0000100" else
                '0';
    en_mov  <=  '1' when opcode = "0001011" else
                '0';
    en_addi <=  '1' when opcode = "0000111" else
                '0';
	en_brlt <=  '1' when opcode = "0111100" and flags="00000100" else
                '0';
	en_cp   <=  '1' when opcode = "0000101" else
                '0';
end architecture;