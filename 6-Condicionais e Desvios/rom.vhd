library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rom is
    port( clk           :  in std_logic;
          endereco      :  in unsigned(7 downto 0); -- define a posicao da ROM que vamos acessar, como vaide 0 a 256 = 8 bits
          dados_rom     :  out unsigned(14 downto 0) -- tamanho de cada dado da ROM
    );
end entity;

architecture a_rom of rom is
    type mem is array (0 to 255) of unsigned(14 downto 0);
    constant conteudo_rom : mem := (
       -- caso endereco => conteudo
       0  => "111011000011110",		-- LDI R6,30;		carrega 30 em R6
       1  => "111001100000000",		-- LDI R3,0;		carrega 0 em R3
       2  => "111010000000000",		-- LDI R4,0;		carrega 0 em R4
       3  => "000000001000011",		-- ADD R4,R3;		soma R3 com R4 e armazena em R4
       4  => "000011100110001",		-- ANDI R3,1;		soma 1 a R3 e armazena em R3
	   5  => "000010100110110",		-- CP R3,R6;		compara R6 com R3 e armazena em uma flag Z
	   6  => "011110011111100",		-- BRLT passo3;		branch relativo que volta para o passo3, coloca um a mais porque o PC soma 1
	   7  => "000101101010100",		-- MOV R5,R4;		copia o valor de R4 para R5
       -- abaixo: casos omissos => (zero em todos os bits)
       others => (others=>'0')
    );
begin
    process(clk)
    begin
        if(rising_edge(clk)) then
            dados_rom <= conteudo_rom(to_integer(endereco));
        end if;
    end process;
end architecture;



