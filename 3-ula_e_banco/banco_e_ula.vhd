library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity banco_e_ula is
	port(	seleciona_operacao		: in unsigned(1 downto 0);
			read_reg1		 		: in unsigned(2 downto 0);
			read_reg2 		 		: in unsigned(2 downto 0);
			write_register 	 		: in unsigned(2 downto 0);
			cte_ula					: in unsigned(15 downto 0);
			wr_en			 		: in std_logic;
			clk				 		: in std_logic;
			rst				 		: in std_logic;
			sel_in2_ula				: in std_logic	
	);
end entity;

architecture a_banco_e_ula of banco_e_ula is
	component bancoderegistradores is
		port(	read_reg1		 : in unsigned(2 downto 0);
				read_reg2 		 : in unsigned(2 downto 0);
				write_data		 : in unsigned(15 downto 0);
				write_register 	 : in unsigned(2 downto 0);
				wr_en			 : in std_logic;
				clk				 : in std_logic;
				rst				 : in std_logic;
				read_data1		 : out unsigned(15 downto 0);
				read_data2		 : out unsigned(15 downto 0)
		);
	end component;
	component ULA
		port(	entrada_dados0				     : in unsigned(15 downto 0);
				entrada_dados1					 : in unsigned(15 downto 0);
				seleciona_operacao				 : in unsigned (1 downto 0);
				saida           			     : out unsigned(15 downto 0)
        );
	end component;
	signal out1_banco, out2_banco,in1_ula, in2_ula, out_ula, entrada_dados_banco	:	unsigned(15 downto 0);
begin
	ula0: ULA port map(	entrada_dados0=>in1_ula,
						entrada_dados1=>in2_ula,
						seleciona_operacao=>seleciona_operacao,
						saida=>out_ula);
	banco0: bancoderegistradores port map(	read_reg1=>read_reg1,
											read_reg2=>read_reg2,
											write_data=>entrada_dados_banco,
											write_register=>write_register,
											wr_en=>wr_en,
											rst=>rst,
											clk=>clk,
											read_data1=>out1_banco,
											read_data2=>out2_banco);
	
	entrada_dados_banco		<= 	out_ula;
	in1_ula					<=	out1_banco;
	in2_ula					<=	out2_banco when sel_in2_ula = '0' else
								cte_ula;
				
end architecture;