library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity banco_e_ula_tb is
end entity;



architecture a_banco_e_ula_tb of banco_e_ula_tb is
	component banco_e_ula
		port(	seleciona_operacao		: in unsigned(1 downto 0);
				read_reg1		 		: in unsigned(2 downto 0);
				read_reg2 		 		: in unsigned(2 downto 0);
				write_register 	 		: in unsigned(2 downto 0);
				cte_ula					: in unsigned(15 downto 0);
				wr_en			 		: in std_logic;
				clk				 		: in std_logic;
				rst				 		: in std_logic;
				sel_in2_ula				: in std_logic	
		);
	end component;
	signal clk, rst, wr_en, sel_in2_ula				: std_logic;
	signal seleciona_operacao						: unsigned(1 downto 0);
	signal read_reg1,read_reg2,write_register	 	: unsigned(2 downto 0);
	signal cte_ula						 			: unsigned(15 downto 0);
	
begin
	uut:	banco_e_ula port map(	clk=>clk,rst=>rst,wr_en=>wr_en,sel_in2_ula=>sel_in2_ula,
									seleciona_operacao=>seleciona_operacao,
									read_reg1=>read_reg1,read_reg2=>read_reg2,write_register=>write_register,
									cte_ula=>cte_ula
								);
process -- sinal clock
	begin
		clk <= '0';
		wait for 50 ns;
		clk <= '1';
		wait for 50 ns;
	end process;
	
	process -- sinal reset
	begin
		rst <= '1';
		wait for 100 ns;
		rst <= '0';
		wait for 1100 ns;
		rst <= '1';
		wait for 100 ns;
		rst <= '0';
		wait;
	end process;
	
	process
	begin
		wait for 100 ns;
		wr_en <= '1';
		sel_in2_ula <= '1';
		seleciona_operacao	<= "00";
		
		cte_ula <= "0000000000001010";
		write_register <= "000";
		wait for 100 ns;
		
		
		cte_ula <= "0000000000000001";
		write_register <= "001";
		wait for 100 ns;
		
        cte_ula <= "0000000000000010";
		write_register <= "010";
		wait for 100 ns;
        
        cte_ula <= "0000000000000011";
		write_register <= "011";
		wait for 100 ns;
        
        cte_ula <= "0000000000000100";
		write_register <= "100";
		wait for 100 ns;
        
        cte_ula <= "0000000000000101";
		write_register <= "101";
		wait for 100 ns;
        
        cte_ula <= "0000000000000110";
		write_register <= "110";
		wait for 100 ns;
        
        cte_ula <= "0000000000000111";
		write_register <= "111";
		wait for 100 ns;
        
        cte_ula <= "0000000000000000";
		wr_en <= '0';
		wait for 100 ns;
		
		read_reg1 <= "000";
		read_reg2 <= "001";
		wait for 100 ns;
		
		sel_in2_ula <= '0';
		wait for 100 ns;
		
		
		
		wait;
	end process;
end architecture;